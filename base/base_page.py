from selenium.webdriver.support.wait import WebDriverWait
from utils import DriverUtil
#封装基类
class BasePage:
    def  __init__(self):
        self.driver=    DriverUtil.get_app_driver()#启动
# 1）元素查找
    def  find_el(self,loc):
        try:
            el=WebDriverWait(self.driver,10).until(lambda x:x.find_element(*loc))
            return el
        except Exception as e:
            print('没有找到元素')
            raise e
#2）元素点击
    def click_el(self,loc):
        self.find_el(loc).click()
#3）文本输入
    def input_text(self,loc,text):
        self.find_el(loc).click()
        self.find_el(loc).send_keys(text)
#4）滑动查找元素
    def swipe_find_el(self,loc,fx='up'):
        #定义w宽高h坐标
        w=self.driver.get_window_size()['width']
        h=self.driver.get_window_size()['height']
        if fx=='up':
            s_x=e_x=w/2
            s_y=h*0.9
            e_y=h*0.1
        elif fx=='down':
            s_x=e_x=w/2
            s_y=h*0.1
            e_y=h*0.9
        elif fx=='left':
            s_x=w*0.9
            e_x=w*0.1
            s_y= e_y=h/2
        else:
            s_x=w*0.1
            e_x=w*0.9
            s_y= e_y=h/2
        while True:
            start_page=self.driver.page_source
            try:
                el=self.find_el(loc )
                print('已找到元素')
                return el
            except Exception as e:
                print('没有找到元素')
                self.driver.swipe(s_x,s_y,e_x,e_y)
            if start_page==self.driver.page_source:
                print('已确定没有找到元素')
                return None