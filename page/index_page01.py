'''1 首页页面
元素：1）我的
my_loc = MobileBy.XPATH, "//*[@text='我的']"
元素：1）搜索框
find_loc=MobileBy.CLASS_NAME,'android.widget.EditText'
业务方法：
go_my_page()
go_find_shop
'''
from appium.webdriver.common.mobileby import MobileBy
from base.base_page import BasePage
class IndexPage(BasePage):
    def __init__(self):
        super().__init__()
        # 我的元素
        self.my_loc = MobileBy.XPATH, "//*[@text='我的']"
    # 点击我的元素进入我的页面
    def go_my_page(self):
        self.click_el(self.my_loc)